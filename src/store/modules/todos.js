import axios from 'axios';

const fakeDataSource = [
    {
        id: 1,
        title: "first",
        completed: true
    },
    {
        id: 2,
        title: "second",
        completed: false
    }
]

const state = {
    todos: [],
    filteredTodos: []
};

const getters = {
    allTodos: (state) => state.todos,
    filteredTodos: (state) => state.filteredTodos
};

const actions = {
    fetchTodos({commit}) {
        commit('getTodos', fakeDataSource)
    },
    addTodo({commit}, title) {
        const todo = {
            title,
            completed: false
        }
        commit('newTodo', todo)
    },
    deleteTodo({commit}, id) {
        commit('removeTodo', id)
    },
    filterCompletedTodos({commit}) {
        commit('filterComplited')
    },
    filterUncompletedTodos({commit}) {
        commit('filterUncomplited')
    },
    updateTodo({commit}, updTodo) {
        commit('updateTodo', updTodo)
    },
    completeAll({commit}) {
        commit('completeAllTodos')
    },
    clearCompleted({commit}) {
        commit('clearAllCompleted')
    },
    clearFilteredTodos({commit}) {
      commit("clearFiltered")
    }
};

const mutations = {
    getTodos: (state, data) => {
        state.todos = data
    },
    newTodo: (state, todo) => {
        todo.id = state.todos.length + 1;
        state.todos.unshift(todo)
    },
    removeTodo: (state, id) => state.todos = state.todos.filter(todo => todo.id !== id),
    updateTodo: (state, updTodo) => {
        const index = state.todos.findIndex(todo => todo.id === updTodo.id);
        if (index !== -1) {
            state.todos.splice(index, 1, updTodo)
        }
    },
    completeAllTodos: (state) => {
        state.todos = state.todos.map(todo => {
            todo.completed = true
            return todo
        })
    },
    clearAllCompleted: (state) => {
        state.todos = state.todos.filter(todo => !todo.completed)
        state.filteredTodos = state.todos.filter(todo => !todo.completed)
    },
    filterComplited: (state) => {
        state.filteredTodos = state.todos.filter(todo => todo.completed)
    },
    filterUncomplited: (state) => {
        state.filteredTodos = state.todos.filter(todo => !todo.completed)
    },
    clearFiltered:(state) => {
      state.filteredTodos = []
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
